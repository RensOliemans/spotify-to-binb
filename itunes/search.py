import json
import os
import time
from json import JSONDecodeError

import requests

from track.track import ForbiddenError, NotFoundError, Track


def search_songs(tracks):
    for track in tracks:
        try:
            result = search_song(track.title, track.artist)
            print(result)
            yield result
            time.sleep(3)  # iTunes API has a rate limit of approx 20 calls/min
        except KeyboardInterrupt:
            print("Stopping with searching for more songs...")
            return
        except requests.exceptions.ConnectionError:
            print("Could not contact iTunes API, stopping...")
            return


def search_song(title, artist):
    uri = build_uri(title, artist)
    with requests.request('GET', uri) as req:
        try:
            body = json.loads(req.content)['results'][0]
            return Track(title, artist, body['trackId'], body['trackViewUrl'],
                         body['previewUrl'], body['artworkUrl60'], body['artworkUrl100'])
        except JSONDecodeError:
            return Track(title, artist, ForbiddenError())
        except IndexError:
            return Track(title, artist, NotFoundError())


def build_uri(title, artist):
    def convert(s):
        return "+".join(s.lower().split())
    return f'{os.getenv("API")}/search?media=music&country=US&limit=1&term={convert(title)}+{convert(artist)}'
