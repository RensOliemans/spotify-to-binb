import os
import sqlite3

from track.track import convert_db_to_track, ForbiddenError, NotFoundError


class DbConnection:
    def __init__(self):
        self._conn = None
        self._cursor = None

    def get_all_tracks(self):
        self.cursor.execute('SELECT title, artist, result, view_url,'
                            'preview_url, artwork_url_60, artwork_url_100 FROM track')
        return [convert_db_to_track(t) for t in self.cursor.fetchall()]

    def get_found_tracks(self):
        self.cursor.execute('SELECT title, artist, result, view_url,'
                            'preview_url, artwork_url_60, artwork_url_100 FROM track '
                            'WHERE NOT has_error')
        return [convert_db_to_track(t) for t in self.cursor.fetchall()]

    def get_forbidden(self):
        self.cursor.execute('SELECT title, artist, result, view_url,'
                            'preview_url, artwork_url_60, artwork_url_100 FROM track '
                            'WHERE has_error')
        tracks = [convert_db_to_track(t) for t in self.cursor.fetchall()]
        return [t for t in tracks if isinstance(t.result, ForbiddenError)]

    def get_not_found(self):
        self.cursor.execute('SELECT title, artist, result, view_url,'
                            'preview_url, artwork_url_60, artwork_url_100 FROM track '
                            'WHERE has_error')
        tracks = [convert_db_to_track(t) for t in self.cursor.fetchall()]
        return [t for t in tracks if isinstance(t.result, NotFoundError)]

    def add_tracks(self, tracks):
        tracks = [(track.title, track.artist, str(track.result), track.view_url,
                   track.preview_url, track.artwork_url_60, track.artwork_url_100, track.has_error)
                  for track in tracks]
        self.cursor.executemany('INSERT INTO track (title, artist, result, view_url,'
                                'preview_url, artwork_url_60, artwork_url_100, has_error)'
                                'VALUES (?, ?, ?, ?, ?, ?, ?, ?)', tracks)

    def filter_existing_tracks(self, tracks):
        all_tracks = self.get_all_tracks()
        return [t for t in tracks if t not in all_tracks]

    @property
    def cursor(self) -> sqlite3.Cursor:
        if self._cursor is None:
            self._cursor = self._conn.cursor()
        return self._cursor

    def commit(self):
        self._conn.commit()

    def __enter__(self):
        self._conn = sqlite3.connect(os.getenv('DBNAME'))
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._conn.commit()
        self._conn.close()
