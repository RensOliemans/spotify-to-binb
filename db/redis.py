import redis


class RedisConnection:
    def __init__(self):
        self.r = None

    def add_tracks(self, tracks):
        tracks = (track for track in tracks if not track.has_error)
        for i, track in enumerate(tracks):
            self.add_track(i + 1, track.artist, track.title, track.view_url, track.preview_url,
                           track.artwork_url_60, track.artwork_url_100, 'vicky', i + 1)

    def add_track(self, song_id, artist, title, track_view_url, track_preview_url, track_artwork_url_60,
                  track_artwork_url_100, room, score):
        self.r.hmset(f'song:{song_id}', {
            'artistName': artist,
            'trackName': title,
            'trackViewUrl': track_view_url,
            'previewUrl': track_preview_url,
            'artworkUrl60': track_artwork_url_60,
            'artworkUrl100': track_artwork_url_100
        })

        self.r.zadd(room, {score: str(song_id)})

    def __enter__(self):
        self.r = redis.Redis(host='localhost', port=6379, db=0)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.r.close()
