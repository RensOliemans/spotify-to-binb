CREATE TABLE IF NOT EXISTS "track" (
 title text,
 artist text,
 result text,
 view_url text,
 preview_url text,
 artwork_url_60 text,
 artwork_url_100 text,
 has_error boolean,
 unique(title, artist));
