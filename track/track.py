class Track:
    def __init__(self, title, artist, result=None, view_url=None, preview_url=None,
                 artwork_url_60=None, artwork_url_100=None):
        self.title = title
        self.artist = artist
        self.result = result
        self.view_url = view_url
        self.preview_url = preview_url
        self.artwork_url_60 = artwork_url_60
        self.artwork_url_100 = artwork_url_100

    @property
    def has_error(self):
        return isinstance(self.result, Error)

    def __str__(self):
        return f"{self.result}, //{self.title}---{self.artist}"

    def __repr__(self):
        return f"<Track. result={self.result}, title={self.title}, artist={self.artist}>"

    def __eq__(self, other):
        return self.title == other.title and self.artist == other.artist

    def __hash__(self):
        return hash(self.title + self.artist)

    @staticmethod
    def from_strings(title, artist, result, view_url, preview_url, art60, art100):
        if result.startswith(ForbiddenError.s):
            result = ForbiddenError()
        elif result.startswith(NotFoundError.s):
            result = NotFoundError()
        return Track(title, artist, result, view_url, preview_url, art60, art100)


def convert_db_to_track(entry):
    title, artist, result, view_url, preview_url, art60, art100 = entry
    return Track.from_strings(title, artist, result, view_url, preview_url, art60, art100)


class Error:
    s = "ERROR: unknown"

    def __str__(self):
        return self.s


class ForbiddenError(Error):
    s = "ERROR: 403"


class NotFoundError(Error):
    s = "ERROR no results"
