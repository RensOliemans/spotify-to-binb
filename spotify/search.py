import spotipy
from spotipy.oauth2 import SpotifyClientCredentials

from track.track import Track


def get_tracks(playlist_id):
    auth_manager = SpotifyClientCredentials()
    sp = spotipy.Spotify(auth_manager=auth_manager)
    return get_items_from_playlist(sp, playlist_id)


def get_items_from_playlist(spotify, playlist_id):
    offset = 0
    items = set()
    while True:
        playlist = spotify.playlist_items(playlist_id, offset=offset)
        new_items = extract_tracks_from_result(playlist)
        if new_items:
            items = items.union(new_items)
            offset += len(new_items)
        else:
            break
    return items


def extract_tracks_from_result(d):
    items = set()
    for s in d['items']:
        track = s['track']
        items.add(Track(track['name'], track['artists'][0]['name']))
    return items
