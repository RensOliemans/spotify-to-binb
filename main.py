import os

from dotenv import load_dotenv

from itunes import search as itunes
from spotify import search as spotify
from db.sqlite import DbConnection
from db.redis import RedisConnection

load_dotenv()


def main():
    tracks = get_new_tracks()
    add_new_tracks_to_redis(tracks)


def get_new_tracks():
    with DbConnection() as db:
        all_spotify_tracks = _get_spotify_tracks(os.getenv('PLAYLIST_ID'))
        tracks_to_add = _filter_existing_tracks(all_spotify_tracks, db)
        _add_new_tracks_to_db(tracks_to_add, db)
        return db.get_found_tracks()


def _get_spotify_tracks(playlist_id):
    print(f'Getting tracks of Spotify playlist...')
    all_spotify_tracks = spotify.get_tracks(playlist_id)
    print(f'Spotify playlist now has {len(all_spotify_tracks)} entries')
    return all_spotify_tracks


def _filter_existing_tracks(tracks, db):
    print(f'We have {len(db.get_all_tracks())} entries in the database')
    new_tracks = db.filter_existing_tracks(tracks)
    print(f'Trying to find and add {len(new_tracks)} new tracks...')
    return new_tracks


def _add_new_tracks_to_db(tracks, db):
    tracks_with_metadata = itunes.search_songs(tracks)
    db.add_tracks(tracks_with_metadata)
    db.commit()
    print(f'Added; now we have {len(db.get_all_tracks())} in the database, of which {len(db.get_found_tracks())} '
          f'have been found on iTunes.')


def add_new_tracks_to_redis(tracks):
    with RedisConnection() as rc:
        rc.add_tracks(tracks)
        print(f'Added tracks to Redis')


if __name__ == '__main__':
    main()
