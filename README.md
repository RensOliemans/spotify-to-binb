# Spotify to Binb
This python library takes a spotify playlist, attempts to find all songs in
iTunes, and adds them to a local Redis database in the format that
[binb](https://binb.co) likes.

## System Requirements
* [Redis](https://redis.io)
* [sqlite](https://sqlite.org/index.html)

## Python requirements
See `requirements.txt`
* redis
* spotipy
* requests

## How it works
### Create a sqlite database
* See `schema.sql` for the schema. Default name is `tracks.db`, see `.env`

### Edit `.env`
* Add Spotify client secret and client id (https://developer.spotify.com/dashboard/applications)
* Add Spotify playlist_id (can be its url)

### Run
`python main.py`


## Flaws / Roadmap
* Only supports single playlist, can be worked around by running multiple times for multiple playlists
* Only supports single binb room, default 'hits' (see `rc.py`)
* Needs sqlite3 database. This is useful since you don't have to get all songs in one go (which can take a while due to
  iTunes rate limiting). I might add support for doing it without a database.
